#!/usr/bin/env bash

set -euo pipefail

influx query 'from(bucket: "metrics") |> range(start:-1m)'
