#!/usr/bin/env bash

set -euo pipefail

hostname=$(hostname)

influx write --bucket metrics "cpu,host=$hostname value=14"
